﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

using System.Text.RegularExpressions;

namespace Overwatch.API.Misc
{
    public static class OverwatchApiHelpers
    {
        public static bool IsValidBattletag(string battletag) => new Regex(@"\w+#\d+").IsMatch(battletag);

        public static string ProfileUrl(string username, Region region, Platform platform) => platform == Platform.Pc
                                                                                                  ? $"https://playoverwatch.com/en-gb/career/{platform.ToString().ToLower()}/{region.ToString().ToLower()}/{username.Replace("#", "-")}"
                                                                                                  : $"https://playoverwatch.com/en-gb/career/{platform.ToString().ToLower()}/{username}";
    }
}