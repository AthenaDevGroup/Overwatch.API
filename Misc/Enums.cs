﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

namespace Overwatch.API.Misc
{
    public enum Region
    {
        Us,
        Eu,
        Kr,
        None
    }

    public enum Platform
    {
        Pc,
        Xbl,
        Psn,
        None
    }

    public enum Mode
    {
        Casual,
        Competitive
    }
}