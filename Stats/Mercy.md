# Mercy
---

- Hero Specific
	- Players Resurrected
	- Players Resurrected - Most in Game
	- Melee Final Blows - Most in Game
	- Players Resurrected - Average

- Combat
	- Eliminations
	- Final Blows
	- Solo Kills
	- Shots Fired
	- Shots Hit
	- Critical Hits
	- Damage Done
	- Objective Kills
	- Melee Final Blows
	- Critical Hits - Average
	- Critical Hit Accuracy
	- Eliminations per Life
	- Weapon Accuracy

- Assists
	- Healing Done
	- Teleporter Pads Destroyed
	- Turrets Destroyed
	- Offensive Assists
	- Self Healing

- Best
	- Eliminations - Most in Life
	- Damage Done - Most in Life
	- Healing Done - Most in Life
	- Weapon Accuracy - Best in Game
	- Kill Streak - Best
	- Damage Done - Most in Game
	- Healing Done - Most in Game
	- Eliminations - Most in Game
	- Final Blows - Most in Game
	- Objective Kills - Most in Game
	- Objective Time - Most in Game
	- Solo Kills - Most in Game
	- Offensive Assists - Most in Game
	- Critical Hits - Most in Game
	- Critical Hits - Most in Life
	- Self Healing - Most in Game

- Average
	- Melee Final Blows - Average
	- Self Healing - Average
	- Offensive Assists - Average
	- Deaths - Average
	- Solo Kills - Average
	- Objective Time - Average
	- Objective Kills - Average
	- Healing Done - Average
	- Final Blows - Average
	- Eliminations - Average
	- Damage Done - Average

- Deaths
	- Deaths
	- Environmental Deaths

- Match Awards
	- Medals - Bronze
	- Medals - Silver
	- Medals - Gold
	- Medals
	- Cards

- Game
	- Time Played
	- Games Won
	- Objective Time
	- Time Spent on Fire

- Miscellaneous
	- Blaster Kills
	- Blaster Kills - Most in Game
	- Defensive Assists
	- Defensive Assists - Most in Game
	- Healing Done
	- Healing Done - Most in Game
	- Damage Amplified
	- Damage Amplified - Most in Game
	- Time Spent on Fire - Most in Game
	- Time Spent on Fire - Average
	- Damage Amplified - Average
	- Healing Done - Average
	- Defensive Assists - Average
	- Blaster Kills - Average
