# Symmetra
---

- Hero Specific
	- Sentry Turret Kills
	- Sentry Turret Kills - Most in Game
	- Players Teleported
	- Players Teleported - Most in Game
	- Shields Provided
	- Shields Provided - Most in Game
	- Teleporter Uptime
	- Teleporter Uptime - Best in Game
	- Melee Final Blow - Most in Game
	- Shields Provided - Average
	- Sentry Turret Kills - Average
	- Players Teleported - Average
	- Teleporter Uptime - Average

- Combat
	- Eliminations
	- Final Blows
	- Solo Kills
	- Damage Done
	- Objective Kills
	- Multikills
	- Melee Final Blow
	- Eliminations per Life

- Assists
	- Teleporter Pad Destroyed
	- Turrets Destroyed

- Best
	- Eliminations - Most in Life
	- Damage Done - Most in Life
	- Kill Streak - Best
	- Damage Done - Most in Game
	- Eliminations - Most in Game
	- Final Blows - Most in Game
	- Objective Kills - Most in Game
	- Objective Time - Most in Game
	- Solo Kills - Most in Game

- Average
	- Melee Final Blows - Average
	- Deaths - Average
	- Solo Kills - Average
	- Objective Time - Average
	- Objective Kills - Average
	- Final Blows - Average
	- Eliminations - Average
	- Damage Done - Average

- Deaths
	- Deaths
	- Environmental Deaths

- Match Awards
	- Medals - Bronze
	- Medals - Silver
	- Medals - Gold
	- Medals
	- Cards

- Game
	- Time Played
	- Games Won
	- Objective Time
	- Time Spent on Fire

- Miscellaneous
	- Multikill - Best
	- Damage Blocked
	- Damage Blocked - Most in Game
	- Shield Generator Destroyed
	- Time Spent on Fire - Most in Game
	- Time Spent on Fire - Average
	- Damage Blocked - Average
