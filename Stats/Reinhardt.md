# Reinhardt
---

- Hero Specific
	- Damage Blocked
	- Damage Blocked - Most in Game
	- Charge Kills
	- Charge Kills - Most in Game
	- Fire Strike Kills
	- Fire Strike Kills - Most in Game
	- Earthshatter Kills
	- Earthshatter Kills - Most in Game
	- Fire Strike Kills - Average
	- Earthshatter Kills - Average
	- Damage Blocked - Average
	- Charge Kills - Average

- Combat
	- Eliminations
	- Final Blows
	- Solo Kills
	- Damage Done
	- Objective Kills
	- Multikills
	- Environmental Kills
	- Eliminations per Life

- Assists
	- Teleporter Pads Destroyed
	- Turrets Destroyed
	- Offensive Assists

- Best
	- Eliminations - Most in Life
	- Damage Done - Most in Life
	- Kill Streak - Best
	- Damage Done - Most in Game
	- Eliminations - Most in Game
	- Final Blows - Most in Game
	- Objective Kills - Most in Game
	- Objective Time - Most in Game
	- Solo Kills - Most in Game
	- Offensive Assists - Most in Game

- Average
	- Offensive Assists - Average
	- Deaths - Average
	- Solo Kills - Average
	- Objective Time - Average
	- Objective Kills - Average
	- Final Blows - Average
	- Eliminations - Average
	- Damage Done - Average

- Deaths
	- Deaths
	- Environmental Deaths

- Match Awards
	- Medals - Bronze
	- Medals - Silver
	- Medals - Gold
	- Medals
	- Cards

- Game
	- Time Played
	- Games Won
	- Objective Time
	- Time Spent on Fire

- Miscellaneous
	- Multikill - Best
	- Shield Generators Destroyed
	- Time Spent on Fire - Most in Game
	- Time Spent on Fire - Average
