# Winston
---

- Hero Specific
	- Players Knocked Back
	- Damage Blocked
	- Damage Blocked - Most in Game
	- Players Knocked Back - Most in Game
	- Melee Kills
	- Melee Kills - Most in Game
	- Jump Pack Kills
	- Jump Pack Kills - Most in Game
	- Melee Final Blows - Most in Game
	- Players Knocked Back - Average
	- Melee Kills - Average
	- Jump Pack Kills - Average
	- Damage Blocked - Average

- Combat
	- Eliminations
	- Final Blows
	- Solo Kills
	- Damage Done
	- Objective Kills
	- Multikills
	- Environmental Kills
	- Melee Final Blows
	- Eliminations per Life

- Assists
	- Teleporter Pad Destroyed
	- Turrets Destroyed

- Best
	- Eliminations - Most in Life
	- Damage Done - Most in Life
	- Kill Streak - Best
	- Damage Done - Most in Game
	- Eliminations - Most in Game
	- Final Blows - Most in Game
	- Objective Kills - Most in Game
	- Objective Time - Most in Game
	- Solo Kills - Most in Game

- Average
	- Melee Final Blows - Average
	- Deaths - Average
	- Solo Kills - Average
	- Objective Time - Average
	- Objective Kills - Average
	- Final Blows - Average
	- Eliminations - Average
	- Damage Done - Average

- Deaths
	- Deaths
	- Environmental Deaths

- Match Awards
	- Medals - Bronze
	- Medals - Silver
	- Medals - Gold
	- Medals
	- Cards

- Game
	- Time Played
	- Games Won
	- Objective Time
	- Time Spent on Fire

- Miscellaneous
	- Multikill - Best
	- Primal Rage Kills
	- Primal Rage Kills - Most in Game
	- Shield Generator Destroyed
	- Time Spent on Fire - Most in Game
	- Time Spent on Fire - Average
	- Primal Rage Kills  - Average
