# ALLHEROES
---

- Combat
	- Melee Final Blows
	- Solo Kills
	- Objective Kills
	- Final Blows
	- Damage Done
	- Eliminations
	- Environmental Kills
	- Multikills

- Assists
	- Healing Done
	- Recon Assists
	- Teleporter Pads Destroyed

- Best
	- Eliminations - Most in Game
	- Final Blows - Most in Game
	- Damage Done - Most in Game
	- Healing Done - Most in Game
	- Defensive Assists - Most in Game
	- Offensive Assists - Most in Game
	- Objective Kills - Most in Game
	- Objective Time - Most in Game
	- Multikill - Best
	- Solo Kills - Most in Game
	- Time Spent on Fire - Most in Game

- Average
	- Melee Final Blows - Average
	- Time Spent on Fire - Average
	- Solo Kills - Average
	- Objective Time - Average
	- Objective Kills - Average
	- Healing Done - Average
	- Final Blows - Average
	- Deaths - Average
	- Damage Done - Average
	- Eliminations - Average

- Deaths
	- Deaths
	- Environmental Deaths

- Match Awards
	- Cards
	- Medals
	- Medals - Gold
	- Medals - Silver
	- Medals - Bronze

- Game
	- Games Won
	- Time Spent on Fire
	- Objective Time
	- Time Played

- Miscellaneous
	- Melee Final Blows - Most in Game
	- Shield Generators Destroyed - Most in Game
	- Turrets Destroyed - Most in Game
	- Environmental Kills - Most in Game
	- Teleporter Pads Destroyed - Most in Game
	- Kill Streak - Best
	- Shield Generators Destroyed
	- Turrets Destroyed
	- Recon Assists - Most in Game
	- Offensive Assists
	- Defensive Assists
