# Torbjorn
---

- Hero Specific
	- Armor Packs Created
	- Torbjörn Kills
	- Turret Kills
	- Torbjörn Kills - Most in Game
	- Molten Core Kills
	- Molten Core Kills - Most in Game
	- Melee Final Blow - Most in Game
	- Turret Kills - Average
	- Torbjörn Kills - Average
	- Molten Core Kills - Average
	- Armor Packs Created - Average

- Combat
	- Eliminations
	- Final Blows
	- Solo Kills
	- Shots Fired
	- Shots Hit
	- Critical Hits
	- Damage Done
	- Objective Kills
	- Multikills
	- Melee Final Blows
	- Critical Hits - Average
	- Critical Hit Accuracy
	- Eliminations per Life
	- Weapon Accuracy

- Assists
	- Turrets Destroyed

- Best
	- Eliminations - Most in Life
	- Damage Done - Most in Life
	- Weapon Accuracy - Best in Game
	- Kill Streak - Best
	- Damage Done - Most in Game
	- Eliminations - Most in Game
	- Final Blows - Most in Game
	- Objective Kills - Most in Game
	- Objective Time - Most in Game
	- Solo Kills - Most in Game
	- Critical Hits - Most in Game
	- Critical Hits - Most in Life

- Average
	- Melee Final Blows - Average
	- Deaths - Average
	- Solo Kills - Average
	- Objective Time - Average
	- Objective Kills - Average
	- Final Blows - Average
	- Eliminations - Average
	- Damage Done - Average

- Deaths
	- Deaths
	- Environmental Deaths

- Match Awards
	- Medals - Bronze
	- Medals - Silver
	- Medals - Gold
	- Medals
	- Cards

- Game
	- Time Played
	- Games Won
	- Objective Time
	- Time Spent on Fire

- Miscellaneous
	- Multikill - Best
	- Time Spent on Fire - Average
