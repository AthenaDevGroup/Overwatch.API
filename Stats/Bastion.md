# Bastion
---

- Hero Specific
	- Recon Kills
	- Sentry Kills
	- Tank Kills
	- Sentry Kills - Most in Game
	- Recon Kills - Most in Game
	- Tank Kills - Most in Game
	- Melee Final Blow - Most in Game
	- Tank Kills - Average
	- Sentry Kills - Average
	- Recon Kills - Average

- Combat
	- Eliminations
	- Final Blows
	- Solo Kills
	- Shots Fired
	- Shots Hit
	- Critical Hits
	- Damage Done
	- Objective Kills
	- Multikills
	- Melee Final Blows
	- Critical Hits - Average
	- Critical Hit Accuracy
	- Eliminations per Life
	- Weapon Accuracy

- Assists
	- Healing Done
	- Teleporter Pad Destroyed
	- Turrets Destroyed
	- Self Healing

- Best
	- Eliminations - Most in Life
	- Damage Done - Most in Life
	- Healing Done - Most in Life
	- Weapon Accuracy - Best in Game
	- Kill Streak - Best
	- Damage Done - Most in Game
	- Healing Done - Most in Game
	- Eliminations - Most in Game
	- Final Blows - Most in Game
	- Objective Kills - Most in Game
	- Objective Time - Most in Game
	- Solo Kills - Most in Game
	- Critical Hits - Most in Game
	- Critical Hits - Most in Life
	- Self Healing - Most in Game

- Average
	- Melee Final Blows - Average
	- Self Healing - Average
	- Deaths - Average
	- Solo Kills - Average
	- Objective Time - Average
	- Objective Kills - Average
	- Healing Done - Average
	- Final Blows - Average
	- Eliminations - Average
	- Damage Done - Average

- Deaths
	- Deaths
	- Environmental Deaths

- Match Awards
	- Medals - Bronze
	- Medals - Silver
	- Medals - Gold
	- Medals
	- Cards

- Game
	- Time Played
	- Games Won
	- Objective Time
	- Time Spent on Fire

- Miscellaneous
	- Multikill - Best
	- Time Spent on Fire - Most in Game
	- Time Spent on Fire - Average
