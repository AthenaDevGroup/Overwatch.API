# Pharah
---

- Hero Specific
	- Rocket Direct Hits
	- Barrage Kills
	- Rocket Direct Hits - Most in Game
	- Barrage Kills - Most in Game
	- Rocket Direct Hits - Average
	- Barrage Kills - Average

- Combat
	- Eliminations
	- Final Blows
	- Solo Kills
	- Shots Fired
	- Shots Hit
	- Damage Done
	- Objective Kills
	- Multikills
	- Environmental Kills
	- Eliminations per Life
	- Weapon Accuracy

- Assists
	- Teleporter Pads Destroyed
	- Turrets Destroyed

- Best
	- Eliminations - Most in Life
	- Damage Done - Most in Life
	- Weapon Accuracy - Best in Game
	- Kill Streak - Best
	- Damage Done - Most in Game
	- Eliminations - Most in Game
	- Final Blows - Most in Game
	- Objective Kills - Most in Game
	- Objective Time - Most in Game
	- Solo Kills - Most in Game

- Average
	- Deaths - Average
	- Solo Kills - Average
	- Objective Time - Average
	- Objective Kills - Average
	- Final Blows - Average
	- Eliminations - Average
	- Damage Done - Average

- Deaths
	- Deaths

- Match Awards
	- Medals - Bronze
	- Medals - Silver
	- Medals - Gold
	- Medals
	- Cards

- Game
	- Time Played
	- Games Won
	- Objective Time
	- Time Spent on Fire

- Miscellaneous
	- Multikill - Best
	- Shield Generator Destroyed
	- Time Spent on Fire - Average
