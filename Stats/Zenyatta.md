# Zenyatta
---

- Hero Specific
	- Transcendence Healing - Best
	- Melee Final Blows - Most in Game

- Combat
	- Eliminations
	- Final Blows
	- Solo Kills
	- Shots Fired
	- Shots Hit
	- Critical Hits
	- Damage Done
	- Objective Kills
	- Multikills
	- Melee Final Blows
	- Critical Hits - Average
	- Critical Hit Accuracy
	- Eliminations per Life
	- Weapon Accuracy

- Assists
	- Healing Done
	- Teleporter Pad Destroyed
	- Turrets Destroyed
	- Offensive Assists
	- Self Healing

- Best
	- Eliminations - Most in Life
	- Damage Done - Most in Life
	- Healing Done - Most in Life
	- Weapon Accuracy - Best in Game
	- Kill Streak - Best
	- Damage Done - Most in Game
	- Healing Done - Most in Game
	- Eliminations - Most in Game
	- Final Blows - Most in Game
	- Objective Kills - Most in Game
	- Objective Time - Most in Game
	- Solo Kills - Most in Game
	- Offensive Assists - Most in Game
	- Critical Hits - Most in Game
	- Critical Hits - Most in Life
	- Self Healing - Most in Game

- Average
	- Melee Final Blows - Average
	- Self Healing - Average
	- Offensive Assists - Average
	- Deaths - Average
	- Solo Kills - Average
	- Objective Time - Average
	- Objective Kills - Average
	- Healing Done - Average
	- Final Blows - Average
	- Eliminations - Average
	- Damage Done - Average

- Deaths
	- Deaths
	- Environmental Deaths

- Match Awards
	- Medals - Bronze
	- Medals - Silver
	- Medals - Gold
	- Medals
	- Cards

- Game
	- Time Played
	- Games Won
	- Objective Time
	- Time Spent on Fire

- Miscellaneous
	- Multikill - Best
	- Transcendence Healing
	- Defensive Assists
	- Defensive Assists - Most in Game
	- Healing Done
	- Healing Done - Most in Game
	- Time Spent on Fire - Most in Game
	- Time Spent on Fire - Average
	- Healing Done - Average
	- Defensive Assists - Average
