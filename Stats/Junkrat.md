# Junkrat
---

- Hero Specific
	- Enemies Trapped - Most in Game
	- Enemies Trapped
	- RIP-Tire Kills - Most in Game
	- RIP-Tire Kills
	- Melee Final Blow - Most in Game
	- Enemies Trapped a Minute

- Combat
	- Eliminations
	- Final Blows
	- Solo Kills
	- Shots Fired
	- Shots Hit
	- Damage Done
	- Objective Kills
	- Multikills
	- Environmental Kill
	- Melee Final Blows
	- Eliminations per Life
	- Weapon Accuracy

- Assists
	- Teleporter Pad Destroyed
	- Turrets Destroyed
	- Offensive Assists

- Best
	- Eliminations - Most in Life
	- Damage Done - Most in Life
	- Weapon Accuracy - Best in Game
	- Kill Streak - Best
	- Damage Done - Most in Game
	- Eliminations - Most in Game
	- Final Blows - Most in Game
	- Objective Kills - Most in Game
	- Objective Time - Most in Game
	- Solo Kills - Most in Game
	- Offensive Assist - Most in Game

- Average
	- Melee Final Blows - Average
	- Offensive Assists - Average
	- Deaths - Average
	- Solo Kills - Average
	- Objective Time - Average
	- Objective Kills - Average
	- Final Blows - Average
	- Eliminations - Average
	- Damage Done - Average

- Deaths
	- Deaths
	- Environmental Deaths

- Match Awards
	- Medals - Bronze
	- Medals - Silver
	- Medals - Gold
	- Medals
	- Cards

- Game
	- Time Played
	- Games Won
	- Objective Time
	- Time Spent on Fire

- Miscellaneous
	- Multikill - Best
	- Time Spent on Fire - Most in Game
	- Time Spent on Fire - Average
	- RIP-Tire Kills - Average
