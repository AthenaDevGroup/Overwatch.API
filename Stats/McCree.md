# McCree
---

- Hero Specific
	- Deadeye Kills
	- Deadeye Kills - Most in Game
	- Fan the Hammer Kills
	- Melee Final Blow - Most in Game
	- Fan the Hammer Kills - Average
	- Deadeye Kills - Average

- Combat
	- Eliminations
	- Final Blows
	- Solo Kills
	- Shots Fired
	- Shots Hit
	- Critical Hits
	- Damage Done
	- Objective Kills
	- Multikills
	- Melee Final Blows
	- Critical Hits - Average
	- Critical Hit Accuracy
	- Eliminations per Life
	- Weapon Accuracy

- Assists
	- Turrets Destroyed

- Best
	- Eliminations - Most in Life
	- Damage Done - Most in Life
	- Weapon Accuracy - Best in Game
	- Kill Streak - Best
	- Damage Done - Most in Game
	- Eliminations - Most in Game
	- Final Blows - Most in Game
	- Objective Kills - Most in Game
	- Objective Time - Most in Game
	- Solo Kills - Most in Game
	- Critical Hits - Most in Game
	- Critical Hits - Most in Life

- Average
	- Melee Final Blows - Average
	- Deaths - Average
	- Solo Kills - Average
	- Objective Time - Average
	- Objective Kills - Average
	- Final Blows - Average
	- Eliminations - Average
	- Damage Done - Average

- Deaths
	- Deaths
	- Environmental Death

- Match Awards
	- Medals - Bronze
	- Medals - Silver
	- Medals - Gold
	- Medals
	- Cards

- Game
	- Time Played
	- Games Won
	- Objective Time
	- Time Spent on Fire

- Miscellaneous
	- Multikill - Best
	- Fan the Hammer Kills - Most in Game
	- Shield Generator Destroyed
	- Time Spent on Fire - Most in Game
	- Time Spent on Fire - Average
