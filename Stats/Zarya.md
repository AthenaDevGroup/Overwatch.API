# Zarya
---

- Hero Specific
	- Damage Blocked
	- Damage Blocked - Most in Game
	- Lifetime Graviton Surge Kills
	- Graviton Surge Kills - Most in Game
	- High Energy Kills - Most in Game
	- High Energy Kills
	- Lifetime Energy Accumulation
	- Energy Maximum
	- Projected Barriers Applied
	- Average Energy - Best in Game
	- Melee Final Blow - Most in Game
	- Projected Barriers Applied - Average
	- High Energy Kills - Average
	- Graviton Surge Kills - Average
	- Damage Blocked - Average
	- Lifetime Average Energy

- Combat
	- Eliminations
	- Final Blows
	- Solo Kills
	- Shots Fired
	- Shots Hit
	- Damage Done
	- Objective Kills
	- Multikills
	- Melee Final Blows
	- Eliminations per Life
	- Weapon Accuracy

- Assists
	- Teleporter Pads Destroyed
	- Turrets Destroyed

- Best
	- Eliminations - Most in Life
	- Damage Done - Most in Life
	- Weapon Accuracy - Best in Game
	- Kill Streak - Best
	- Damage Done - Most in Game
	- Eliminations - Most in Game
	- Final Blows - Most in Game
	- Objective Kills - Most in Game
	- Objective Time - Most in Game
	- Solo Kills - Most in Game

- Average
	- Melee Final Blows - Average
	- Deaths - Average
	- Solo Kills - Average
	- Objective Time - Average
	- Objective Kills - Average
	- Final Blows - Average
	- Eliminations - Average
	- Damage Done - Average

- Deaths
	- Deaths
	- Environmental Deaths

- Match Awards
	- Medals - Bronze
	- Medals - Silver
	- Medals - Gold
	- Medals
	- Cards

- Game
	- Time Played
	- Games Won
	- Objective Time
	- Time Spent on Fire

- Miscellaneous
	- Multikill - Best
	- Projected Barriers Applied - Most in Game
	- Shield Generator Destroyed
	- Time Spent on Fire - Most in Game
	- Time Spent on Fire - Average
