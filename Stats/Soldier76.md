# Soldier76
---

- Hero Specific
	- Helix Rockets Kills - Most in Game
	- Helix Rockets Kills
	- Tactical Visor Kills
	- Tactical Visor Kills - Most in Game
	- Biotic Fields Deployed
	- Biotic Field Healing Done
	- Melee Final Blows - Most in Game
	- Tactical Visor Kills - Average
	- Helix Rockets Kills - Average

- Combat
	- Eliminations
	- Final Blows
	- Solo Kills
	- Shots Fired
	- Shots Hit
	- Critical Hits
	- Damage Done
	- Objective Kills
	- Multikills
	- Melee Final Blows
	- Critical Hits - Average
	- Critical Hit Accuracy
	- Eliminations per Life
	- Weapon Accuracy

- Assists
	- Healing Done
	- Teleporter Pads Destroyed
	- Turrets Destroyed
	- Self Healing

- Best
	- Eliminations - Most in Life
	- Damage Done - Most in Life
	- Healing Done - Most in Life
	- Weapon Accuracy - Best in Game
	- Kill Streak - Best
	- Damage Done - Most in Game
	- Healing Done - Most in Game
	- Eliminations - Most in Game
	- Final Blows - Most in Game
	- Objective Kills - Most in Game
	- Objective Time - Most in Game
	- Solo Kills - Most in Game
	- Critical Hits - Most in Game
	- Critical Hits - Most in Life
	- Self Healing - Most in Game

- Average
	- Melee Final Blows - Average
	- Self Healing - Average
	- Deaths - Average
	- Solo Kills - Average
	- Objective Time - Average
	- Objective Kills - Average
	- Healing Done - Average
	- Final Blows - Average
	- Eliminations - Average
	- Damage Done - Average

- Deaths
	- Deaths
	- Environmental Deaths

- Match Awards
	- Medals - Bronze
	- Medals - Silver
	- Medals - Gold
	- Medals
	- Cards

- Game
	- Time Played
	- Games Won
	- Objective Time
	- Time Spent on Fire

- Miscellaneous
	- Multikill - Best
	- Healing Done
	- Healing Done - Most in Game
	- Shield Generators Destroyed
	- Time Spent on Fire - Most in Game
	- Time Spent on Fire - Average
	- Healing Done - Average
