# Sombra
---

- Hero Specific
	- Melee Final Blows - Most in Game

- Combat
	- Eliminations
	- Final Blows
	- Solo Kills
	- Shots Fired
	- Shots Hit
	- Critical Hits
	- Damage Done
	- Objective Kills
	- Melee Final Blows
	- Eliminations per Life
	- Weapon Accuracy

- Assists
	- Healing Done
	- Turrets Destroyed
	- Offensive Assists

- Best
	- Eliminations - Most in Life
	- Damage Done - Most in Life
	- Healing Done - Most in Life
	- Weapon Accuracy - Best in Game
	- Kill Streak - Best
	- Damage Done - Most in Game
	- Healing Done - Most in Game
	- Eliminations - Most in Game
	- Final Blows - Most in Game
	- Objective Kills - Most in Game
	- Objective Time - Most in Game
	- Solo Kills - Most in Game
	- Offensive Assists - Most in Game

- Average
	- Melee Final Blows - Average
	- Offensive Assists - Average
	- Deaths - Average
	- Solo Kills - Average
	- Objective Time - Average
	- Objective Kills - Average
	- Healing Done - Average
	- Final Blows - Average
	- Eliminations - Average
	- Damage Done - Average

- Deaths
	- Deaths
	- Environmental Deaths

- Match Awards
	- Medals - Bronze
	- Medals - Silver
	- Medals - Gold
	- Medals
	- Cards

- Game
	- Time Played
	- Games Won
	- Objective Time
	- Time Spent on Fire

- Miscellaneous
	- Enemies Hacked
	- Enemies EMP'd
	- Enemies Hacked - Most in Game
	- Enemies EMP'd - Most in Game
	- Time Spent on Fire - Average
	- Enemies EMP'd - Average
	- Enemies Hacked - Average
