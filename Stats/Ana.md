# Ana
---


- Hero Specific
	- Scoped Hits
	- Scoped Shots
	- Scoped Accuracy - Best in Game
	- Melee Final Blow - Most in Game
	- Nano Boosts Applied
	- Nano Boost Assists
	- Nano Boost Assists - Most in Game
	- Nano Boosts Applied - Average
	- Nano Boost Assists - Average
	- Unscoped Accuracy
	- Scoped Accuracy

- Combat
	- Eliminations
	- Final Blows
	- Solo Kills
	- Shots Fired
	- Shots Hit
	- Damage Done
	- Objective Kills
	- Melee Final Blow
	- Eliminations per Life
	- Weapon Accuracy

- Assists
	- Healing Done
	- Turrets Destroyed
	- Offensive Assists
	- Self Healing

- Best
	- Eliminations - Most in Life
	- Damage Done - Most in Life
	- Healing Done - Most in Life
	- Weapon Accuracy - Best in Game
	- Kill Streak - Best
	- Damage Done - Most in Game
	- Healing Done - Most in Game
	- Eliminations - Most in Game
	- Final Blows - Most in Game
	- Objective Kills - Most in Game
	- Objective Time - Most in Game
	- Solo Kills - Most in Game
	- Offensive Assists - Most in Game
	- Self Healing - Most in Game

- Average
	- Melee Final Blows - Average
	- Self Healing - Average
	- Offensive Assists - Average
	- Deaths - Average
	- Solo Kills - Average
	- Objective Time - Average
	- Objective Kills - Average
	- Healing Done - Average
	- Final Blows - Average
	- Eliminations - Average
	- Damage Done - Average

- Deaths
	- Deaths
	- Environmental Deaths

- Match Awards
	- Medals - Bronze
	- Medals - Silver
	- Medals - Gold
	- Medals
	- Cards

- Game
	- Time Played
	- Games Won
	- Objective Time
	- Time Spent on Fire

- Miscellaneous
	- Defensive Assists
	- Defensive Assists - Most in Game
	- Healing Done
	- Healing Done - Most in Game
	- Unscoped Shots
	- Unscoped Hits
	- Enemies Slept
	- Unscoped Accuracy - Best in Game
	- Enemies Slept - Most in Game
	- Nano Boosts Applied - Most in Game
	- Time Spent on Fire - Average
	- Enemies Slept - Average
	- Healing Done - Average
	- Defensive Assists - Average
