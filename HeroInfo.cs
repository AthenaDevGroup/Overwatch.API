﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

using System.Collections.Generic;

namespace Overwatch.API
{
    public static class HeroInfo
    {
        private static readonly List<string> Heroes = new List<string>
        {
            "ana",
            "bastion",
            "dva",
            "d.va",
            "doomfist",
            "genji",
            "hanzo",
            "junkrat",
            "lucio",
            "mccree",
            "mei",
            "mercy",
            "orisa",
            "pharah",
            "reaper",
            "reinhardt",
            "roadhog",
            "soldier76",
            "sombra",
            "symmetra",
            "torbjorn",
            "tracer",
            "widowmaker",
            "winston",
            "zarya",
            "zenyatta"
        };

        public static string NiceName(string hero)
        {
            switch (hero)
            {
                case "ana":
                    return "Ana";
                case "bastion":
                    return "Bastion";
                case "dva":
                case "d.va":
                    return "D.Va";
                case "doomfist":
                    return "Doomfist";
                case "genji":
                    return "Genji";
                case "hanzo":
                    return "Hanzo";
                case "junkrat":
                    return "Junkrat";
                case "lucio":
                    return "Lúcio";
                case "mccree":
                    return "McCree";
                case "mei":
                    return "Mei";
                case "mercy":
                    return "Mercy";
                case "orisa":
                    return "Orisa";
                case "pharah":
                    return "Pharah";
                case "reaper":
                    return "Reaper";
                case "reinhardt":
                    return "Reinhardt";
                case "roadhog":
                    return "Roadhog";
                case "soldier76":
                    return "Soldier 76";
                case "sombra":
                    return "Sombra";
                case "symmetra":
                    return "Symmetra";
                case "torbjorn":
                    return "Torbjörn";
                case "tracer":
                    return "Tracer";
                case "widowmaker":
                    return "Widowmaker";
                case "winston":
                    return "Winston";
                case "zarya":
                    return "Zarya";
                case "zenyatta":
                    return "Zenyatta";
                default:
                    return string.Empty;
            }
        }

        public static bool HeroExists(string hero) => Heroes.Contains(hero.ToLower());

        public static string GetImg(string hero)
        {
            hero = hero.ToLower();
            if (hero.Equals("soldier76"))
                hero = "soldier-76";
            return $"https://blzgdapipro-a.akamaihd.net/hero/{hero}/icon-portrait.png";
        }
    }
}