﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

using System;
using System.Net.Http;
using System.Threading.Tasks;

using AngleSharp;
using AngleSharp.Dom;

using Overwatch.API.Misc;

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable UnusedMember.Global

namespace Overwatch.API
{
    public sealed class Player
    {
        /// <summary>
        ///     Construct a new Overwatch player.
        /// </summary>
        /// <param name="username">The players Battletag (SomeUser#1234) or Username for PSN/XBL</param>
        /// <param name="platform">
        ///     The players platform - Defaults to "none" if a battletag is not given (use DetectPlatform() if
        ///     platform is not known)
        /// </param>
        /// <param name="region">
        ///     The players region (only required for PC) - Defaults to "none" (use DetectRegionPC if region is
        ///     not known)
        /// </param>
        public Player(string username, Platform platform = Platform.Pc, Region region = Region.Eu)
        {
            Username = username;
            Platform = platform;
            if (!OverwatchApiHelpers.IsValidBattletag(username) && platform == Platform.Pc)
                throw new InvalidBattletagException();

            if (OverwatchApiHelpers.IsValidBattletag(username))
            {
                Platform = Platform.Pc;
                BattletagUrlFriendly = username.Replace("#", "-");
                Region = region;
            }
            if (Region != Region.None && Platform != Platform.None)
                ProfileUrl = OverwatchApiHelpers.ProfileUrl(Username, Region, Platform);
        }

        /// <summary>
        ///     The players Battletag with Discriminator - e.g. "SomeUser#1234"
        /// </summary>
        public string Username { get; }

        /// <summary>
        ///     The PlayOverwatch profile of the player - This is only available if the user has set a region
        /// </summary>
        public string ProfileUrl { get; private set; }

        /// <summary>
        ///     The Player Level of the player
        /// </summary>
        public ushort PlayerLevel { get; private set; }

        /// <summary>
        ///     The Competitive Rank of the player
        /// </summary>
        public ushort CompetitiveRank { get; private set; }

        /// <summary>
        ///     The Competitive Rank Image of the player
        /// </summary>
        public string CompetitiveImg { get; private set; }

        /// <summary>
        ///     The player's region - EU/US/None
        /// </summary>
        public Region Region { get; private set; }

        /// <summary>
        ///     The player's platform - PC/XBL/PSN
        /// </summary>
        public Platform Platform { get; private set; }

        /// <summary>
        ///     The players quick-play stats.
        /// </summary>
        public OverwatchStats CasualStats { get; private set; }

        /// <summary>
        ///     The players competitive stats.
        /// </summary>
        public OverwatchStats CompetitiveStats { get; private set; }

        /// <summary>
        ///     The players achievements.
        /// </summary>
        public Achievements Achievements { get; private set; }

        /// <summary>
        ///     The last time the profile was downloaded from PlayOverwatch.
        /// </summary>
        public DateTime ProfileLastDownloaded { get; private set; }

        /// <summary>
        ///     A direct link to the users profile portrait.
        /// </summary>
        public string ProfilePortraitUrl { get; private set; }

        /// <summary>
        ///     The URL friendly version of the users Battletag.
        /// </summary>
        private string BattletagUrlFriendly { get; }

        /// <summary>
        ///     Detect the region of the player - This method will simply return if the player is not on PC. - [SLOW]
        /// </summary>
        /// <returns></returns>
        public async Task DetectRegionPc()
        {
            if (Platform != Platform.Pc)
                return;

            var baseUrl = "http://playoverwatch.com/en-gb/career/";
            var naAppend = $"pc/us/{BattletagUrlFriendly}";
            var euAppend = $"pc/eu/{BattletagUrlFriendly}";
            var krAppend = $"pc/kr/{BattletagUrlFriendly}";
            var client = new HttpClient {BaseAddress = new Uri(baseUrl)};
            using (var responseNa = await client.GetAsync(naAppend))
            {
                if (responseNa.IsSuccessStatusCode)
                {
                    Region = Region.Us;
                    ProfileUrl = baseUrl + naAppend;
                    return;
                }
            }

            using (var responseEu = await client.GetAsync(euAppend))
            {
                if (responseEu.IsSuccessStatusCode)
                {
                    Region = Region.Eu;
                    ProfileUrl = baseUrl + euAppend;
                    return;
                }
            }

            using (var responseKr = await client.GetAsync(krAppend))
            {
                if (responseKr.IsSuccessStatusCode)
                {
                    Region = Region.Kr;
                    ProfileUrl = baseUrl + krAppend;
                    return;
                }
            }

            Region = Region.None;
        }

        /// <summary>
        ///     Detect the platform of the player [SLOW]
        /// </summary>
        /// <returns></returns>
        public async Task DetectPlatform()
        {
            if (OverwatchApiHelpers.IsValidBattletag(Username))
            {
                Platform = Platform.Pc;
                return;
            }

            var baseUrl = "http://playoverwatch.com/en-gb/career/";
            var psnAppend = $"psn/{Username}";
            var xblAppend = $"xbl/{Username}";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);
                using (var responsePsn = await client.GetAsync(psnAppend))
                {
                    if (responsePsn.IsSuccessStatusCode)
                    {
                        Platform = Platform.Psn;
                        ProfileUrl = baseUrl + psnAppend;
                        return;
                    }
                }

                using (var responseXbl = await client.GetAsync(xblAppend))
                {
                    if (responseXbl.IsSuccessStatusCode)
                    {
                        Platform = Platform.Xbl;
                        ProfileUrl = baseUrl + xblAppend;
                        return;
                    }
                }
            }

            Platform = Platform.None;
        }

        /// <summary>
        ///     Downloads and parses the players profile
        /// </summary>
        /// <returns></returns>
        public async Task UpdateStats()
        {
            if (Region == Region.None && Platform == Platform.Pc)
                throw new UserRegionNotDefinedException();
            if (Platform == Platform.None)
                throw new UserPlatformNotDefinedException();

            var userpage = await DownloadUserPage();
            GetUserRanks(userpage);
            GetProfilePortrait(userpage);
            CasualStats = new OverwatchStats();
            CompetitiveStats = new OverwatchStats();
            Achievements = new Achievements();
            Achievements.UpdateAchievementsFromPage(userpage);
            CasualStats.UpdateStatsFromPage(userpage, Mode.Casual);
            CompetitiveStats.UpdateStatsFromPage(userpage, Mode.Competitive);
            ProfileLastDownloaded = DateTime.UtcNow;
        }

        internal void GetUserRanks(IDocument doc)
        {
            PlayerLevel = 0;
            CompetitiveRank = 0;
            CompetitiveImg = string.Empty;
            if (ushort.TryParse(doc.QuerySelector("div.player-level div")?.TextContent, out ushort parsedPlayerLevel))
                PlayerLevel = parsedPlayerLevel;
            var playerLevelImageId = StaticVars.PlayerRankImageRegex
                .Match(doc.QuerySelector("div.player-level")?.GetAttribute("style")).Value;
            PlayerLevel += StaticVars.PrestigeDefinitions[playerLevelImageId];
            if (ushort.TryParse(doc.QuerySelector("div.competitive-rank div")?.TextContent, out ushort parsedCompetitiveRank))
                CompetitiveRank = parsedCompetitiveRank;
            var compImg = doc.QuerySelector("div.competitive-rank img")?.OuterHtml;
            if (!string.IsNullOrEmpty(compImg))
                CompetitiveImg = compImg?.Replace("<img src=\"", "").Replace("\">", "");
        }

        internal void GetProfilePortrait(IDocument doc)
        {
            ProfilePortraitUrl = doc.QuerySelector(".player-portrait").GetAttribute("src");
        }

        internal async Task<IDocument> DownloadUserPage()
        {
            var config = Configuration.Default.WithDefaultLoader();
            if (ProfileUrl == null)
                throw new UserProfileUrlNullException();

            return await BrowsingContext.New(config).OpenAsync(ProfileUrl);
        }
    }
}