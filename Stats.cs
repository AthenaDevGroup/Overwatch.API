﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

using AngleSharp.Dom;

using Overwatch.API.Misc;

namespace Overwatch.API
{
    public sealed class OverwatchStats : List<Hero>
    {
        /// <summary>
        ///     Get a hero by name.
        /// </summary>
        /// <param name="name">The name of the hero</param>
        /// <returns>A hero object if one by such a name exists - otherwise null.</returns>
        public Hero GetHero(string name)
        {
            return this.FirstOrDefault(x => string.Compare(name, x.Name, StringComparison.OrdinalIgnoreCase) == 0);
        }

        internal void UpdateStatsFromPage(IDocument doc, Mode mode)
        {
            var divModeId = "";
            switch (mode)
            {
                case Mode.Casual:
                    divModeId = "quickplay";
                    break;
                case Mode.Competitive:
                    divModeId = "competitive";
                    break;
            }

            var innerContent = doc.QuerySelector($"div[id='{divModeId}']");
            var idDictionary = new Dictionary<string, string>();
            foreach (var dropdownitem in innerContent.QuerySelectorAll("select > option"))
            {
                var id = dropdownitem.GetAttribute("value");
                if (id.StartsWith("0x0", StringComparison.Ordinal))
                    idDictionary.Add(id, ParseHeroName(dropdownitem.TextContent));
            }
            foreach (var section in innerContent.QuerySelectorAll("div[data-group-id='stats']"))
            {
                var catId = section.GetAttribute("data-category-id");
                var hero = new Hero(idDictionary[catId]);
                Add(hero);
                foreach (var table in section.QuerySelectorAll($"div[data-category-id='{catId}'] table.data-table"))
                {
                    var cat = new StatCategory(table.QuerySelector("thead").TextContent);
                    var statDict = new Dictionary<string, double>();
                    foreach (var row in table.QuerySelectorAll("tbody tr")
                        .Where(row => !statDict.ContainsKey(row.Children[0].TextContent)))
                        statDict.Add(row.Children[0].TextContent, OwValToDouble(row.Children[1].TextContent));

                    cat.AddRange(statDict.Select(x => new Stat(x.Key, x.Value)));
                    hero.Add(cat);
                }
            }
        }

        private static double OwValToDouble(string input)
        {
            if (input.ToLower().Contains("hour"))
                return TimeSpan.FromHours(int.Parse(input.Substring(0, input.IndexOf(" ", StringComparison.Ordinal))))
                    .TotalSeconds;
            if (input.ToLower().Contains("minute"))
                return TimeSpan.FromMinutes(int.Parse(input.Substring(0, input.IndexOf(" ", StringComparison.Ordinal))))
                    .TotalSeconds;
            if (input.ToLower().Contains("second"))
                return
                    TimeSpan.FromSeconds(Math.Round(
                        double.Parse(input.Substring(0, input.IndexOf(" ", StringComparison.Ordinal))), 2,
                        MidpointRounding.AwayFromZero)).TotalSeconds;

            if (input.Contains(":"))
            {
                if (TimeSpan.TryParseExact(input, @"mm\:ss", CultureInfo.CurrentCulture, out var outputTime))
                    return outputTime.TotalSeconds;
                if (TimeSpan.TryParseExact(input, @"hh\:mm\:ss", CultureInfo.CurrentCulture, out outputTime))
                    return outputTime.TotalSeconds;
            }

            try
            {
                return double.Parse(input.Replace(",", "").Replace("%", ""));
            }
            catch
            {
                return 0;
            }
        }

        public static string ParseHeroName(string input) => input.Replace("ú", "u").Replace(":", "").Replace(" ", "")
            .Replace("ö", "o").Replace(".", "");
    }

    public sealed class Hero : List<StatCategory>
    {
        internal Hero(string n) => Name = n;

        internal string Name { get; }

        /// <summary>
        ///     Get a category by name
        /// </summary>
        /// <param name="name">The name of the category.</param>
        /// <returns>A category object if one by such a name exists - otherwise null.</returns>
        public StatCategory GetCategory(string name)
        {
            var ret = this.FirstOrDefault(x => string.Compare(name, x.Name, StringComparison.OrdinalIgnoreCase) == 0);
            return ret ?? new StatCategory("null");
        }
    }

    public sealed class StatCategory : List<Stat>
    {
        internal StatCategory(string n) => Name = n;

        internal string Name { get; }

        /// <summary>
        ///     Get a stat by name.
        /// </summary>
        /// <param name="name">The name of the stat.</param>
        /// <returns>A stat object if one by such a name exists - otherwise null.</returns>
        public Stat GetStat(string name)
        {
            var ret = this.FirstOrDefault(x => string.Compare(name, x.Name, StringComparison.OrdinalIgnoreCase) == 0);
            return ret ?? new Stat("unavailable", 0);
        }
    }

    public sealed class Stat
    {
        internal Stat(string n, double v)
        {
            Name = n;
            Value = v;
        }

        public string Name { get; }
        public double Value { get; }
    }
}