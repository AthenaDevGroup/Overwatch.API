﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;

using AngleSharp.Dom;

// ReSharper disable UnusedMember.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable MemberCanBePrivate.Global

namespace Overwatch.API
{
    public sealed class Achievements : List<AchievementCategory>
    {
        /// <summary>
        ///     Get an achievement category by name.
        /// </summary>
        /// <param name="name">The name of the achievement category/</param>
        /// <returns>An achievement category object if one by such a name exists - otherwise null.</returns>
        public AchievementCategory GetCategory(string name)
        {
            return this.FirstOrDefault(x => string.Compare(x.Name, name, StringComparison.OrdinalIgnoreCase) == 0);
        }

        internal void UpdateAchievementsFromPage(IDocument doc)
        {
            var idDictionary = new Dictionary<string, string>();
            var innerContent = doc.QuerySelector("section[id='achievements-section']");
            foreach (var dropdownitem in innerContent.QuerySelectorAll("select > option"))
                idDictionary.Add(dropdownitem.GetAttribute("value"), dropdownitem.GetAttribute("option-id"));
            foreach (var item in idDictionary)
            {
                var achievementBlock = innerContent.QuerySelector($"div[data-category-id='{item.Key}']");
                var cat = new AchievementCategory(item.Value);
                Add(cat);
                foreach (var achievement in achievementBlock.QuerySelectorAll("div.achievement-card"))
                    cat.Add(new Achievement(achievement.QuerySelector("div.media-card-title").TextContent,
                        !achievement.GetAttribute("class").Contains("m-disabled")));
            }
        }
    }

    public sealed class AchievementCategory : List<Achievement>
    {
        internal AchievementCategory(string n) => Name = n;

        internal string Name { get; }

        /// <summary>
        ///     Get an achievement by name.
        /// </summary>
        /// <param name="name">The name of the achievement/</param>
        /// <returns>An achievement object if one by such a name exists - otherwise null.</returns>
        public Achievement GetAchievement(string name)
        {
            return this.FirstOrDefault(x => string.Compare(x.Name, name, StringComparison.OrdinalIgnoreCase) == 0);
        }
    }

    public sealed class Achievement
    {
        internal Achievement(string n, bool u)
        {
            Name = n;
            IsUnlocked = u;
        }

        internal string Name { get; }
        public bool IsUnlocked { get; }
    }
}